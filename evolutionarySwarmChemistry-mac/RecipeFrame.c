// RecipeFrame.c


//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>
//#include <gtk/gtk.h>
//#include "SwarmPopulationSimulator.h"
//#include "Recipe.h"
//
//typedef struct {
//    Recipe *recipe;
//    GdkPixbuf *im;
//    GtkWidget *swarmCanvas;
//    GtkWidget *leftPanel, *rightPanel;
//    GtkWidget *recipeBox;
//    GtkWidget *applyEdits;
//    int width, height;
//    SwarmPopulationSimulator *parentFrame;
//    GList *recipeFrames;
//} RecipeFrame;
//
//static void on_apply_edits(GtkWidget *widget, gpointer data);
//static gboolean on_delete_event(GtkWidget *widget, GdkEvent *event, gpointer data);
//
//RecipeFrame* recipe_frame_new(SwarmPopulationSimulator *par, GList *swarmInAnyOrder, int w, int h, GList *rcfs) {
//    RecipeFrame *rf = g_malloc(sizeof(RecipeFrame));
//    rf->parentFrame = par;
//    rf->width = w;
//    rf->height = h;
//    rf->recipeFrames = rcfs;
//
//    GtkWidget *window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
//    gtk_window_set_title(GTK_WINDOW(window), "Recipe of Swarm");
//    gtk_window_set_default_size(GTK_WINDOW(window), 600, 240);
//    g_signal_connect(window, "delete-event", G_CALLBACK(on_delete_event), rf);
//
//    rf->rightPanel = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
//    gtk_box_pack_start(GTK_BOX(rf->rightPanel), gtk_label_new("Screen shot"), FALSE, FALSE, 0);
//    rf->swarmCanvas = gtk_drawing_area_new();
//    gtk_widget_set_size_request(rf->swarmCanvas, 200, 200);
//    gtk_box_pack_start(GTK_BOX(rf->rightPanel), rf->swarmCanvas, TRUE, TRUE, 0);
//    gtk_container_add(GTK_CONTAINER(window), rf->rightPanel);
//
//    rf->leftPanel = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
//    gtk_box_pack_start(GTK_BOX(rf->leftPanel), gtk_label_new("Format: # of agents * (R, Vn, Vm, c1, c2, c3, c4, c5)"), FALSE, FALSE, 0);
//    rf->recipeBox = gtk_text_view_new();
//    gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(rf->recipeBox), GTK_WRAP_WORD);
//    gtk_box_pack_start(GTK_BOX(rf->leftPanel), rf->recipeBox, TRUE, TRUE, 0);
//    rf->applyEdits = gtk_button_new_with_label("Apply edits");
//    g_signal_connect(rf->applyEdits, "clicked", G_CALLBACK(on_apply_edits), rf);
//    gtk_box_pack_start(GTK_BOX(rf->leftPanel), rf->applyEdits, FALSE, FALSE, 0);
//    gtk_container_add(GTK_CONTAINER(window), rf->leftPanel);
//
//    rf->recipe = recipe_new(swarmInAnyOrder);
//    GtkTextBuffer *buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(rf->recipeBox));
//    gtk_text_buffer_set_text(buffer, rf->recipe->recipeText, -1);
//
//    gtk_widget_show_all(window);
//    return rf;
//}
//
//static void on_apply_edits(GtkWidget *widget, gpointer data) {
//    RecipeFrame *rf = (RecipeFrame *)data;
//    GtkTextBuffer *buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(rf->recipeBox));
//    GtkTextIter start, end;
//    gtk_text_buffer_get_start_iter(buffer, &start);
//    gtk_text_buffer_get_end_iter(buffer, &end);
//    gchar *text = gtk_text_buffer_get_text(buffer, &start, &end, FALSE);
//
//    recipe_set_from_text(rf->recipe, text);
//    gtk_text_buffer_set_text(buffer, rf->recipe->recipeText, -1);
//
//    if (rf->recipe->recipeText[0] == '*') {
//        gtk_text_view_set_background_color(GTK_TEXT_VIEW(rf->recipeBox), &(GdkRGBA){1.0, 1.0, 0.0, 1.0});
//    } else {
//        gtk_text_view_set_background_color(GTK_TEXT_VIEW(rf->recipeBox), &(GdkRGBA){1.0, 1.0, 1.0, 1.0});
//        SwarmPopulation *newSwarmPop = swarm_population_new(rf->recipe->createPopulation(rf->width, rf->height), "Created from a given recipe");
//        swarm_population_simulator_replace_population_with(rf->parentFrame, newSwarmPop);
//        gdk_pixbuf_fill(rf->im, 0x00000000);
//        gtk_widget_queue_draw(rf->swarmCanvas);
//    }
//
//    g_free(text);
//}
//
//static gboolean on_delete_event(GtkWidget *widget, GdkEvent *event, gpointer data) {
//    RecipeFrame *rf = (RecipeFrame *)data;
//    rf->recipeFrames = g_list_remove(rf->recipeFrames, rf->parentFrame->displayedRecipe);
//    rf->parentFrame->displayedRecipe = NULL;
//    g_free(rf);
//    return FALSE;
//}
//
//void recipe_frame_orphanize(RecipeFrame *rf) {
//    gtk_window_set_title(GTK_WINDOW(rf->leftPanel), "Orphaned recipe");
//    gtk_text_view_set_editable(GTK_TEXT_VIEW(rf->recipeBox), FALSE);
//    gtk_widget_set_sensitive(rf->applyEdits, FALSE);
//}
//
//
