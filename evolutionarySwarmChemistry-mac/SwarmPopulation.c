// SwarmPopulation.c

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

#define MAX_POPULATION 10000

typedef struct {
    double neighborhoodRadius;
    double normalSpeed;
    double maxSpeed;
    double c1, c2, c3, c4, c5;
    int genome;
} SwarmParameters;

typedef struct {
    double x, y, vx, vy;
    SwarmParameters params;
} SwarmIndividual;

typedef struct {
    SwarmIndividual *population;
    int populationSize;
    char *title;
} SwarmPopulation;

double random_double(double min, double max) {
    return min + (rand() / (RAND_MAX / (max - min)));
}

double shorten(double d) {
    return round(d * 100.0) / 100.0;
}

SwarmPopulation* createSwarmPopulationFromRecipe(const char *recipeText, const char *title) {
    SwarmPopulation *sp = (SwarmPopulation*)malloc(sizeof(SwarmPopulation));
    sp->population = (SwarmIndividual*)malloc(MAX_POPULATION * sizeof(SwarmIndividual));
    sp->populationSize = 300; // Assuming createPopulation creates 300 individuals
    sp->title = strdup(title);

    int num = 10000 - 1;
    double xx, yy;

    for (int i = 0; i < num; i++) {
        xx = random_double(-2500, 2500);
        yy = random_double(-2500, 2500);
        SwarmParameters params = {10, 0, 0, 0, 0, 0, 0, 1};
        sp->population[sp->populationSize++] = (SwarmIndividual){xx + 150, yy + 150, 0, 0, params};
    }

    return sp;
}

SwarmPopulation* createSwarmPopulation(int n, const char *title) {
    SwarmPopulation *sp = (SwarmPopulation*)malloc(sizeof(SwarmPopulation));
    sp->population = (SwarmIndividual*)malloc(MAX_POPULATION * sizeof(SwarmIndividual));
    sp->populationSize = 0;
    sp->title = strdup(title);

    int num = 10000 - n;
    double xx, yy;

    for (int i = 0; i < n; i++) {
        xx = random_double(-2500, 2500);
        yy = random_double(-2500, 2500);
        SwarmParameters spParams = {0}; // Assuming default constructor
        sp->population[sp->populationSize++] = (SwarmIndividual){xx + 150, yy + 150, 0, 0, spParams};
    }

    for (int i = 0; i < num; i++) {
        xx = random_double(-2500, 2500);
        yy = random_double(-2500, 2500);
        SwarmParameters params = {10, 0, 0, 0, 0, 0, 0, 1};
        sp->population[sp->populationSize++] = (SwarmIndividual){xx + 150, yy + 150, 0, 0, params};
    }

    return sp;
}

SwarmPopulation* createSwarmPopulationFromAnother(SwarmPopulation *a, const char *title) {
    SwarmPopulation *sp = (SwarmPopulation*)malloc(sizeof(SwarmPopulation));
    sp->population = (SwarmIndividual*)malloc(MAX_POPULATION * sizeof(SwarmIndividual));
    sp->populationSize = 0;
    sp->title = strdup(title);

    for (int i = 0; i < a->populationSize; i++) {
        SwarmIndividual temp = a->population[i];
        SwarmParameters params = temp.params;
        sp->population[sp->populationSize++] = (SwarmIndividual){
            random_double(-2500, 2500) + 150,
            random_double(-2500, 2500) + 150,
            random_double(-5, 5),
            random_double(-5, 5),
            params
        };
    }

    return sp;
}

SwarmPopulation* createSwarmPopulationFromTwo(SwarmPopulation *a, SwarmPopulation *b, double rate, const char *title) {
    SwarmPopulation *sp = (SwarmPopulation*)malloc(sizeof(SwarmPopulation));
    sp->population = (SwarmIndividual*)malloc(MAX_POPULATION * sizeof(SwarmIndividual));
    sp->populationSize = 0;
    sp->title = strdup(title);

    int totalSize = (a->populationSize + b->populationSize) / 2;

    for (int i = 0; i < totalSize; i++) {
        SwarmPopulation *source = (random_double(0, 1) < rate) ? a : b;
        SwarmIndividual temp = source->population[(int)random_double(0, source->populationSize)];
        SwarmParameters params = temp.params;
        sp->population[sp->populationSize++] = (SwarmIndividual){
            random_double(-2500, 2500) + 150,
            random_double(-2500, 2500) + 150,
            random_double(-5, 5),
            random_double(-5, 5),
            params
        };
    }

    return sp;
}

void perturbSwarmPopulation(SwarmPopulation *sp, double pcm, int spaceSize) {
    int pop = sp->populationSize;
    pop += (int)((random_double(-1, 1)) * pcm * (double)pop);
    if (pop < 1) pop = 1;
    if (pop > MAX_POPULATION) pop = MAX_POPULATION;

    SwarmIndividual *newPopulation = (SwarmIndividual*)malloc(pop * sizeof(SwarmIndividual));
    for (int i = 0; i < pop; i++) {
        SwarmParameters tempParam = sp->population[(int)random_double(0, sp->populationSize)].params;
        newPopulation[i] = (SwarmIndividual){
            random_double(0, spaceSize),
            random_double(0, spaceSize),
            random_double(-5, 5),
            random_double(-5, 5),
            tempParam
        };
    }

    free(sp->population);
    sp->population = newPopulation;
    sp->populationSize = pop;
}

//int main() {
//    srand((unsigned int)time(NULL));
//    // Example usage
//    SwarmPopulation *sp = createSwarmPopulation(100, "Example Title");
//    perturbSwarmPopulation(sp, 0.1, 5000);
//    // Free memory
//    free(sp->population);
//    free(sp->title);
//    free(sp);
//    return 0;
//}


