
// Recipe.c

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define MAX_INGREDIENTS 100

typedef struct {
    double neighborhoodRadius;
    double normalSpeed;
    double maxSpeed;
    double c1, c2, c3, c4, c5;
} SwarmParameters;

typedef struct {
    SwarmParameters genome;
    double x, y, vx, vy;
} SwarmIndividual;

typedef struct {
    SwarmParameters parameters[MAX_INGREDIENTS];
    int popCounts[MAX_INGREDIENTS];
    int numIngredients;
    char recipeText[1024];
    double populationChangeMagnitude;
    double duplicationOrDeletionRatePerParameterSets;
    double randomAdditionRatePerRecipe;
    double pointMutationRatePerParameter;
    double pointMutationMagnitude;
} Recipe;

#define isdigit(num) ((num >= '0') && (num <= '9'))

void setFromText(Recipe *recipe, const char *text) {
    char *token, *saveptr, *recipeProcessed = malloc(strlen(text) + 1);
    int i, j, numIngredients, numIndividuals;
    double neighborhoodRadius, normalSpeed, maxSpeed, c1, c2, c3, c4, c5;

    strcpy(recipeProcessed, text);
    for (i = 0, j = 0; recipeProcessed[i]; i++) {
        if (isdigit(recipeProcessed[i]) || recipeProcessed[i] == '.') {
            recipeProcessed[j++] = recipeProcessed[i];
        } else if (j > 0 && recipeProcessed[j - 1] != ' ') {
            recipeProcessed[j++] = ' ';
        }
    }
    recipeProcessed[j] = '\0';

    token = strtok_r(recipeProcessed, " ", &saveptr);
    numIngredients = 0;
    while (token != NULL) {
        numIngredients++;
        for (i = 0; i < 8; i++) {
            token = strtok_r(NULL, " ", &saveptr);
        }
        token = strtok_r(NULL, " ", &saveptr);
    }

    if (numIngredients == 0 || numIngredients > MAX_INGREDIENTS) {
        strcpy(recipe->recipeText, "*** Formatting error ***\n");
        strcat(recipe->recipeText, text);
        recipe->numIngredients = 0;
        free(recipeProcessed);
        return;
    }

    token = strtok_r(recipeProcessed, " ", &saveptr);
    for (i = 0; i < numIngredients; i++) {
        numIndividuals = atoi(token);
        if (numIndividuals < 1) numIndividuals = 1;
        token = strtok_r(NULL, " ", &saveptr);
        neighborhoodRadius = atof(token);
        token = strtok_r(NULL, " ", &saveptr);
        normalSpeed = atof(token);
        token = strtok_r(NULL, " ", &saveptr);
        maxSpeed = atof(token);
        token = strtok_r(NULL, " ", &saveptr);
        c1 = atof(token);
        token = strtok_r(NULL, " ", &saveptr);
        c2 = atof(token);
        token = strtok_r(NULL, " ", &saveptr);
        c3 = atof(token);
        token = strtok_r(NULL, " ", &saveptr);
        c4 = atof(token);
        token = strtok_r(NULL, " ", &saveptr);
        c5 = atof(token);
        recipe->parameters[i].neighborhoodRadius = neighborhoodRadius;
        recipe->parameters[i].normalSpeed = normalSpeed;
        recipe->parameters[i].maxSpeed = maxSpeed;
        recipe->parameters[i].c1 = c1;
        recipe->parameters[i].c2 = c2;
        recipe->parameters[i].c3 = c3;
        recipe->parameters[i].c4 = c4;
        recipe->parameters[i].c5 = c5;
        recipe->popCounts[i] = numIndividuals;
    }

    recipe->numIngredients = numIngredients;
    free(recipeProcessed);
}

void setRecipeText(Recipe *recipe) {
    int i;
    sprintf(recipe->recipeText, "");
    for (i = 0; i < recipe->numIngredients; i++) {
        char line[256];
        sprintf(line, "%d * (%.2f, %.2f, %.2f, %.2f, %.2f, %.2f, %.2f, %.2f)\n",
                recipe->popCounts[i],
                recipe->parameters[i].neighborhoodRadius,
                recipe->parameters[i].normalSpeed,
                recipe->parameters[i].maxSpeed,
                recipe->parameters[i].c1,
                recipe->parameters[i].c2,
                recipe->parameters[i].c3,
                recipe->parameters[i].c4,
                recipe->parameters[i].c5);
        strcat(recipe->recipeText, line);
    }
}

SwarmIndividual *createPopulation(Recipe *recipe, int width, int height) {
    SwarmIndividual *population = malloc(sizeof(SwarmIndividual));
    int i = rand() % recipe->numIngredients;
    population[0].x = 150;
    population[0].y = 150;
    population[0].vx = rand() / (double)RAND_MAX * 10 - 5;
    population[0].vy = rand() / (double)RAND_MAX * 10 - 5;
    population[0].genome = recipe->parameters[i];
    return population;
}

Recipe *mutate(Recipe *recipe) {
    Recipe *newRecipe = malloc(sizeof(Recipe));
    int i, j, numIngredients;
    SwarmParameters tempParam;

    setRecipeText(recipe);
    memcpy(newRecipe, recipe, sizeof(Recipe));

    numIngredients = newRecipe->numIngredients;

    // Insertions, duplications and deletions
    for (j = 0; j < numIngredients; j++) {
        if (rand() / (double)RAND_MAX < newRecipe->duplicationOrDeletionRatePerParameterSets) {
            if (rand() / (double)RAND_MAX < 0.5) { // Duplication
                for (i = numIngredients; i > j; i--) {
                    newRecipe->parameters[i] = newRecipe->parameters[i - 1];
                    newRecipe->popCounts[i] = newRecipe->popCounts[i - 1];
                }
                newRecipe->parameters[j] = recipe->parameters[j];
                newRecipe->popCounts[j] = recipe->popCounts[j];
                numIngredients++;
            } else { // Deletion
                if (numIngredients > 1) {
                    for (i = j; i < numIngredients - 1; i++) {
                        newRecipe->parameters[i] = newRecipe->parameters[i + 1];
                        newRecipe->popCounts[i] = newRecipe->popCounts[i + 1];
                    }
                    numIngredients--;
                    j--;
                }
            }
        }
    }

    if (rand() / (double)RAND_MAX < newRecipe->randomAdditionRatePerRecipe) { // Addition
        newRecipe->parameters[numIngredients].neighborhoodRadius = rand() / (double)RAND_MAX;
        newRecipe->parameters[numIngredients].normalSpeed = rand() / (double)RAND_MAX;
        newRecipe->parameters[numIngredients].maxSpeed = rand() / (double)RAND_MAX;
        newRecipe->parameters[numIngredients].c1 = rand() / (double)RAND_MAX;
        newRecipe->parameters[numIngredients].c2 = rand() / (double)RAND_MAX;
        newRecipe->parameters[numIngredients].c3 = rand() / (double)RAND_MAX;
        newRecipe->parameters[numIngredients].c4 = rand() / (double)RAND_MAX;
        newRecipe->parameters[numIngredients].c5 = rand() / (double)RAND_MAX;
        newRecipe->popCounts[numIngredients] = (int)(rand() / (double)RAND_MAX * MAX_INGREDIENTS * 0.5) + 1;
        numIngredients++;
    }

    newRecipe->numIngredients = numIngredients;

    // Point Mutations
    for (j = 0; j < numIngredients; j++) {
        tempParam = newRecipe->parameters[j];
        if (rand() / (double)RAND_MAX < newRecipe->pointMutationRatePerParameter) {
            tempParam.neighborhoodRadius += (rand() / (double)RAND_MAX * 2 - 1) * newRecipe->pointMutationMagnitude;
        }
        if (rand() / (double)RAND_MAX < newRecipe->pointMutationRatePerParameter) {
            tempParam.normalSpeed += (rand() / (double)RAND_MAX * 2 - 1) * newRecipe->pointMutationMagnitude;
        }
        if (rand() / (double)RAND_MAX < newRecipe->pointMutationRatePerParameter) {
            tempParam.maxSpeed += (rand() / (double)RAND_MAX * 2 - 1) * newRecipe->pointMutationMagnitude;
        }
        if (rand() / (double)RAND_MAX < newRecipe->pointMutationRatePerParameter) {
            tempParam.c1 += (rand() / (double)RAND_MAX * 2 - 1) * newRecipe->pointMutationMagnitude;
        }
        if (rand() / (double)RAND_MAX < newRecipe->pointMutationRatePerParameter) {
            tempParam.c2 += (rand() / (double)RAND_MAX * 2 - 1) * newRecipe->pointMutationMagnitude;
        }
        if (rand() / (double)RAND_MAX < newRecipe->pointMutationRatePerParameter) {
            tempParam.c3 += (rand() / (double)RAND_MAX * 2 - 1) * newRecipe->pointMutationMagnitude;
        }
        if (rand() / (double)RAND_MAX < newRecipe->pointMutationRatePerParameter) {
            tempParam.c4 += (rand() / (double)RAND_MAX * 2 - 1) * newRecipe->pointMutationMagnitude;
        }
        if (rand() / (double)RAND_MAX < newRecipe->pointMutationRatePerParameter) {
            tempParam.c5 += (rand() / (double)RAND_MAX * 2 - 1) * newRecipe->pointMutationMagnitude;
        }
        newRecipe->parameters[j] = tempParam;
    }

    setRecipeText(newRecipe);
    return newRecipe;
}
