// SwarmParameters.c

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

#define numberOfIndividualsMax 10000

double neighborhoodRadiusMax = 300;
double normalSpeedMax = 20;
double maxSpeedMax = 40;
double c1Max = 1;
double c2Max = 1;
double c3Max = 100;
double c4Max = 0.5;
double c5Max = 1;

typedef struct {
    double neighborhoodRadius;
    double normalSpeed;
    double maxSpeed;
    double c1;
    double c2;
    double c3;
    double c4;
    double c5;
} SwarmParameters;

double random_double() {
    return (double)rand() / RAND_MAX;
}

void boundParameterValues(SwarmParameters *params) {
    if (params->neighborhoodRadius < 0) params->neighborhoodRadius = 0;
    else if (params->neighborhoodRadius > neighborhoodRadiusMax) params->neighborhoodRadius = neighborhoodRadiusMax;

    if (params->normalSpeed < 0) params->normalSpeed = 0;
    else if (params->normalSpeed > normalSpeedMax) params->normalSpeed = normalSpeedMax;

    if (params->maxSpeed < 0) params->maxSpeed = 0;
    else if (params->maxSpeed > maxSpeedMax) params->maxSpeed = maxSpeedMax;

    if (params->c1 < 0) params->c1 = 0;
    else if (params->c1 > c1Max) params->c1 = c1Max;

    if (params->c2 < 0) params->c2 = 0;
    else if (params->c2 > c2Max) params->c2 = c2Max;

    if (params->c3 < 0) params->c3 = 0;
    else if (params->c3 > c3Max) params->c3 = c3Max;

    if (params->c4 < 0) params->c4 = 0;
    else if (params->c4 > c4Max) params->c4 = c4Max;

    if (params->c5 < 0) params->c5 = 0;
    else if (params->c5 > c5Max) params->c5 = c5Max;
}

SwarmParameters* createSwarmParameters() {
    SwarmParameters *params = (SwarmParameters*)malloc(sizeof(SwarmParameters));
    params->neighborhoodRadius = random_double() * neighborhoodRadiusMax;
    params->normalSpeed = random_double() * normalSpeedMax;
    params->maxSpeed = random_double() * maxSpeedMax;
    params->c1 = random_double() * c1Max;
    params->c2 = random_double() * c2Max;
    params->c3 = random_double() * c3Max;
    params->c4 = random_double() * c4Max;
    params->c5 = random_double() * c5Max;
    return params;
}

SwarmParameters* createSwarmParametersWithValues(double p1, double p2, double p3, double p4, double p5, double p6, double p7, double p8) {
    SwarmParameters *params = (SwarmParameters*)malloc(sizeof(SwarmParameters));
    params->neighborhoodRadius = p1;
    params->normalSpeed = p2;
    params->maxSpeed = p3;
    params->c1 = p4;
    params->c2 = p5;
    params->c3 = p6;
    params->c4 = p7;
    params->c5 = p8;
    boundParameterValues(params);
    return params;
}

SwarmParameters* copySwarmParameters(SwarmParameters *parent) {
    SwarmParameters *params = (SwarmParameters*)malloc(sizeof(SwarmParameters));
    params->neighborhoodRadius = parent->neighborhoodRadius;
    params->normalSpeed = parent->normalSpeed;
    params->maxSpeed = parent->maxSpeed;
    params->c1 = parent->c1;
    params->c2 = parent->c2;
    params->c3 = parent->c3;
    params->c4 = parent->c4;
    params->c5 = parent->c5;
    return params;
}

bool equals(SwarmParameters *param1, SwarmParameters *param2) {
    return (param1->neighborhoodRadius == param2->neighborhoodRadius) &&
           (param1->normalSpeed == param2->normalSpeed) &&
           (param1->maxSpeed == param2->maxSpeed) &&
           (param1->c1 == param2->c1) &&
           (param1->c2 == param2->c2) &&
           (param1->c3 == param2->c3) &&
           (param1->c4 == param2->c4) &&
           (param1->c5 == param2->c5);
}

void inducePointMutations(SwarmParameters *params, double rate, double magnitude) {
    if (random_double() < rate)
        params->neighborhoodRadius += (random_double() - 0.5) * neighborhoodRadiusMax * magnitude;

    if (random_double() < rate)
        params->normalSpeed += (random_double() - 0.5) * normalSpeedMax * magnitude;

    if (random_double() < rate)
        params->maxSpeed += (random_double() - 0.5) * maxSpeedMax * magnitude;

    if (random_double() < rate)
        params->c1 += (random_double() - 0.5) * c1Max * magnitude;

    if (random_double() < rate)
        params->c2 += (random_double() - 0.5) * c2Max * magnitude;

    if (random_double() < rate)
        params->c3 += (random_double() - 0.5) * c3Max * magnitude;

    if (random_double() < rate)
        params->c4 += (random_double() - 0.5) * c4Max * magnitude;

    if (random_double() < rate)
        params->c5 += (random_double() - 0.5) * c5Max * magnitude;

    boundParameterValues(params);
}

//int main() {
//    srand((unsigned int)time(NULL));
//
//    SwarmParameters *params = createSwarmParameters();
//    SwarmParameters *paramsWithValues = createSwarmParametersWithValues(100, 10, 20, 0.5, 0.5, 50, 0.25, 0.5);
//    SwarmParameters *copiedParams = copySwarmParameters(params);
//
//    printf("Parameters created with random values:\n");
//    printf("neighborhoodRadius: %f\n", params->neighborhoodRadius);
//    printf("normalSpeed: %f\n", params->normalSpeed);
//    printf("maxSpeed: %f\n", params->maxSpeed);
//    printf("c1: %f\n", params->c1);
//    printf("c2: %f\n", params->c2);
//    printf("c3: %f\n", params->c3);
//    printf("c4: %f\n", params->c4);
//    printf("c5: %f\n", params->c5);
//
//    printf("\nParameters created with specific values:\n");
//    printf("neighborhoodRadius: %f\n", paramsWithValues->neighborhoodRadius);
//    printf("normalSpeed: %f\n", paramsWithValues->normalSpeed);
//    printf("maxSpeed: %f\n", paramsWithValues->maxSpeed);
//    printf("c1: %f\n", paramsWithValues->c1);
//    printf("c2: %f\n", paramsWithValues->c2);
//    printf("c3: %f\n", paramsWithValues->c3);
//    printf("c4: %f\n", paramsWithValues->c4);
//    printf("c5: %f\n", paramsWithValues->c5);
//
//    printf("\nCopied parameters:\n");
//    printf("neighborhoodRadius: %f\n", copiedParams->neighborhoodRadius);
//    printf("normalSpeed: %f\n", copiedParams->normalSpeed);
//    printf("maxSpeed: %f\n", copiedParams->maxSpeed);
//    printf("c1: %f\n", copiedParams->c1);
//    printf("c2: %f\n", copiedParams->c2);
//    printf("c3: %f\n", copiedParams->c3);
//    printf("c4: %f\n", copiedParams->c4);
//    printf("c5: %f\n", copiedParams->c5);
//
//    free(params);
//    free(paramsWithValues);
//    free(copiedParams);
//
//    return 0;
//}


