// SwarmChemistryEnvironment.c

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct {
    int width;
    int height;
} Dimension;

typedef struct {
    int top;
    int bottom;
    int left;
    int right;
} Insets;

typedef struct {
    int x;
    int y;
} Point;

typedef struct {
    int numberOfIndividualsMax;
} SwarmParameters;

typedef struct {
    int population;
    char* title;
} SwarmPopulation;

typedef struct {
    int size;
    SwarmParameters* parameters;
    int* popCounts;
} Recipe;

typedef struct {
    int isSelected;
    bool notYetNoticed;
    int displayedRecipe;
} SwarmPopulationSimulator;

typedef struct {
    int isSelected;
    bool notYetNoticed;
} RecipeFrame;

typedef struct {
    int defaultFrameSize;
    int initialSpaceSize;
    int numberOfFrames;
    int showroomW;
    int showroomH;
    int maxFrames;
    bool resettingRequest;
    bool numberOfFramesChangeRequest;
    bool applicationRunning;
    bool thisIsApplet;
    int numberOfSeeds;
    bool givenWithSpecificRecipe;
    char* givenRecipeText;
    SwarmPopulation* populations;
    SwarmPopulationSimulator* sample;
    SwarmPopulationSimulator* selectedSample[2];
    RecipeFrame* recipeFrames;
    long previousTime;
    long currentTime;
    long milliSecPerFrame;
    double populationChangeMagnitude;
    double duplicationOrDeletionRatePerParameterSets;
    double randomAdditionRatePerRecipe;
    double pointMutationRatePerParameter;
    double pointMutationMagnitude;
    char* initcond;
} SwarmChemistryEnvironment;

void constructControlFrame(SwarmChemistryEnvironment* env) {
    printf("Constructing control frame...\n");
    // Code for constructing control frame
}

void determineFrameArrangement(SwarmChemistryEnvironment* env) {
    printf("Determining frame arrangement...\n");
    // Code for determining frame arrangement
}

void createPopulations(SwarmChemistryEnvironment* env) {
    printf("Creating populations...\n");
    // Code for creating populations
}

void createFrames(SwarmChemistryEnvironment* env) {
    printf("Creating frames...\n");
    // Code for creating frames
}

void disposeFrames(SwarmChemistryEnvironment* env) {
    printf("Disposing frames...\n");
    // Code for disposing frames
}

void actionPerformed(SwarmChemistryEnvironment* env, char* source) {
    printf("Action performed: %s\n", source);
    // Code for handling action events
}

void swarmChemistryEnvironment_init(SwarmChemistryEnvironment* env, bool app, char* recipeText) {
    printf("Initializing SwarmChemistryEnvironment...\n");
//    env->controlFrame = (Frame*)malloc(sizeof(Frame));
//    env->screenDimension = (Dimension*)malloc(sizeof(Dimension));
//    env->controlFrameDimension = (Dimension*)malloc(sizeof(Dimension));
//    env->simulatorDimension = (Dimension*)malloc(sizeof(Dimension));
//    env->screenInsets = (Insets*)malloc(sizeof(Insets));
//    env->controlFrameInsets = (Insets*)malloc(sizeof(Insets));
//    env->numberOfFramesText = (TextField*)malloc(sizeof(TextField));
//    env->tracking = (Checkbox*)malloc(sizeof(Checkbox));
//    env->pausing = (Checkbox*)malloc(sizeof(Checkbox));
//    env->mutation = (Checkbox*)malloc(sizeof(Checkbox));
//    env->mouseEffect = (Checkbox*)malloc(sizeof(Checkbox));
//    env->undoing = (Button*)malloc(sizeof(Button));
//    env->resetting = (Button*)malloc(sizeof(Button));
//    env->showing = (Button*)malloc(sizeof(Button));
//    env->quitting = (Button*)malloc(sizeof(Button));
    env->populations = (SwarmPopulation*)malloc(sizeof(SwarmPopulation));
    env->sample = (SwarmPopulationSimulator*)malloc(sizeof(SwarmPopulationSimulator));
    env->selectedSample[0] = (SwarmPopulationSimulator*)malloc(sizeof(SwarmPopulationSimulator));
    env->selectedSample[1] = (SwarmPopulationSimulator*)malloc(sizeof(SwarmPopulationSimulator));
    env->recipeFrames = (RecipeFrame*)malloc(sizeof(RecipeFrame));
    env->initcond = (char*)malloc(sizeof(char));
    env->givenRecipeText = (char*)malloc(sizeof(char));
    env->previousTime = 0;
    env->currentTime = 0;
    env->milliSecPerFrame = 70;
    env->populationChangeMagnitude = 0.8;
    env->duplicationOrDeletionRatePerParameterSets = 0.1;
    env->randomAdditionRatePerRecipe = 0.1;
    env->pointMutationRatePerParameter = 0.1;
    env->pointMutationMagnitude = 0.5;
    env->thisIsApplet = app;
    env->givenWithSpecificRecipe = false;
    env->givenRecipeText = recipeText;
    env->numberOfFrames = 1;
    env->numberOfSeeds = 1;
    env->maxFrames = 1;
    env->resettingRequest = false;
    env->numberOfFramesChangeRequest = false;
    env->applicationRunning = true;
    env->initcond = "random";
    constructControlFrame(env);
    determineFrameArrangement(env);
    createPopulations(env);
    while (env->applicationRunning) {
        int timestep = 0;
        int endtime = 30000;
        int imageoutputinterval = 10;
        createFrames(env);
        int howManySelected = 0;
        env->resettingRequest = env->numberOfFramesChangeRequest = false;
        while (howManySelected < 2 && !env->resettingRequest && !env->numberOfFramesChangeRequest) {
            for (int i = 0; i < env->numberOfFrames; i++) {
                if (env->sample[i].isDisplayable) {
                    if (!env->pausing->getState) {
                        // Code for simulating swarm behavior and updating states
                    }
                    timestep++;
                    if (timestep % imageoutputinterval == 0) {
                        // Code for displaying states and saving image
                    }
                    if (timestep >= endtime) {
                        // Code for finishing simulation
                    }
                    if (env->sample[i].isSelected && env->sample[i].notYetNoticed) {
                        env->sample[i].notYetNoticed = false;
                        env->selectedSample[howManySelected] = &env->sample[i];
                        howManySelected++;
                    }
                    else if (!env->sample[i].isSelected && !env->sample[i].notYetNoticed) {
                        env->sample[i].notYetNoticed = true;
                        howManySelected--;
                    }
                }
                else {
                    if (env->sample[i].isSelected && !env->sample[i].notYetNoticed) {
                        env->sample[i].isSelected = false;
                        env->sample[i].notYetNoticed = true;
                        howManySelected--;
                    }
                }
            }
        }
        disposeFrames(env);
        if (env->resettingRequest) {
            createPopulations(env);
        }
        else if (env->numberOfFramesChangeRequest) {
            int n = atoi(env->numberOfFramesText->getText);
            if (n < 1) {
                n = 1;
            }
            else if (n > env->maxFrames) {
                n = env->maxFrames;
            }
            env->numberOfFramesText->setText(itoa(n));
            if (n != env->numberOfFrames) {
                env->numberOfFramesChangeRequest = true;
            }
        }
        else {
            for (int i = 0; i < env->numberOfFrames; i++) {
                if (i == 0) {
                    env->populations[i] = env->selectedSample[0]->population;
                }
                else if (i == 1 && env->selectedSample[0] != env->selectedSample[1]) {
                    env->populations[i] = env->selectedSample[1]->population;
                }
                else if (i == env->numberOfFrames - 1 && env->numberOfFrames > 3) {
                    env->populations[i] = (SwarmPopulation) {
                        .population = (int)(rand() % SwarmParameters.numberOfIndividualsMax) + 1,
                        .title = "Randomly generated"
                    };
                }
                else {
                    if (env->selectedSample[0] == env->selectedSample[1]) {
                        env->populations[i] = env->selectedSample[0]->population;
                        perturb(&env->populations[i], env->populationChangeMagnitude, env->initialSpaceSize);
                    }
                    else {
                        env->populations[i] = mix(env->selectedSample[0]->population, env->selectedSample[1]->population, (double)(rand() % 6 + 2) / 10);
                    }
                    if (env->mutation->getState) {
                        bool mutated = false;
                        Recipe tempRecipe = getRecipe(env->populations[i].population);
                        int numberOfIngredients = tempRecipe.size;
                        for (int j = 0; j < numberOfIngredients; j++) {
                            if ((double)rand() / RAND_MAX < env->duplicationOrDeletionRatePerParameterSets) {
                                if ((double)rand() / RAND_MAX < 0.5) {
                                    mutated = true;
                                    insert(&tempRecipe, j + 1, tempRecipe.parameters[j], tempRecipe.popCounts[j]);
                                    numberOfIngredients++;
                                    j++;
                                }
                                else {
                                    if (numberOfIngredients > 1) {
                                        mutated = true;
                                        remove(&tempRecipe, j);
                                        numberOfIngredients--;
                                        j--;
                                    }
                                }
                            }
                        }
                        if ((double)rand() / RAND_MAX < env->randomAdditionRatePerRecipe) {
                            mutated = true;
                            add(&tempRecipe, (SwarmParameters) {
                                .width = env->defaultFrameSize,
                                .height = env->defaultFrameSize
                            }, (int)(rand() % SwarmParameters.numberOfIndividualsMax * 0.5) + 1);
                        }
                        for (int j = 0; j < numberOfIngredients; j++) {
                            SwarmParameters tempParam = tempRecipe.parameters[j];
                            inducePointMutations(&tempParam, env->pointMutationRatePerParameter, env->pointMutationMagnitude);
                            if (!equals(tempRecipe.parameters[j], tempParam)) {
                                mutated = true;
                                tempRecipe.parameters[j] = tempParam;
                            }
                        }
                        boundPopulationSize(&tempRecipe);
                        env->populations[i].population = createPopulation(&tempRecipe, env->defaultFrameSize, env->defaultFrameSize);
                        if (mutated) {
                            strcat(env->populations[i].title, " & mutated");
                        }
                    }
                }
            }
        }
    }
}

//int main() {
//    SwarmChemistryEnvironment env;
//    swarmChemistryEnvironment_init(&env, true, "");
//    return 0;
//}


