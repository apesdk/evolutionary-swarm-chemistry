// SwarmIndividual.c

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef struct {
    // Define the structure of SwarmParameters here
    // For example:
    // int someParameter;
    // double anotherParameter;
} SwarmParameters;

typedef struct {
    // Define the structure of Recipe here
    // For example:
    // int someRecipeParameter;
} Recipe;

typedef struct {
    double x, y, dx, dy, dx2, dy2;
    SwarmParameters genome;
    int rankInXOrder, rankInYOrder;
    Recipe *recipe;
} SwarmIndividual;

void initSwarmIndividual(SwarmIndividual *ind, double xx, double yy, double dxx, double dyy, SwarmParameters g, Recipe *r) {
    ind->x = xx;
    ind->y = yy;
    ind->dx = ind->dx2 = dxx;
    ind->dy = ind->dy2 = dyy;
    ind->genome = g;
    ind->rankInXOrder = 0;
    ind->rankInYOrder = 0;
    ind->recipe = r;
}

void accelerate(SwarmIndividual *ind, double ax, double ay, double maxMove) {
    ind->dx2 += ax;
    ind->dy2 += ay;

    double d = ind->dx2 * ind->dx2 + ind->dy2 * ind->dy2;
    if (d > maxMove * maxMove) {
        double normalizationFactor = maxMove / sqrt(d);
        ind->dx2 *= normalizationFactor;
        ind->dy2 *= normalizationFactor;
    }
}

void swarmMove(SwarmIndividual *ind) {
    ind->dx = ind->dx2;
    ind->dy = ind->dy2;
    ind->x += ind->dx;
    ind->y += ind->dy;

    if (ind->x > 2500 + 150) ind->x -= 5000;
    if (ind->x < -2500 + 150) ind->x += 5000;
    if (ind->y > 2500 + 150) ind->y -= 5000;
    if (ind->y < -2500 + 150) ind->y += 5000;
}

// Placeholder for the displayColor function
// You need to define the displayColor function in SwarmParameters
// For example:
// Color displayColor(SwarmParameters *genome) {
//     // Return the color based on the genome
// }

//int main() {
//    // Example usage
//    SwarmParameters genome;
//    Recipe recipe;
//    SwarmIndividual ind;
//    initSwarmIndividual(&ind, 0.0, 0.0, 0.0, 0.0, genome, &recipe);
//
//    // Perform operations on the individual
//    accelerate(&ind, 1.0, 1.0, 10.0);
//    swarmMove(&ind);
//
//    // Display color (assuming displayColor function is defined)
//    // Color color = displayColor(&ind.genome);
//
//    return 0;
//}


