// Recipe.c

#include "recipe.h"

void initializeRecipeFromText(Recipe *recipe, const char *text) {
    recipe->parameters = (SwarmParameters *)malloc(MAX_PARAMETERS * sizeof(SwarmParameters));
    recipe->popCounts = (int *)malloc(MAX_PARAMETERS * sizeof(int));
    recipe->recipeText = strdup(text);
    recipe->populationChangeMagnitude = 0.8;
    recipe->duplicationOrDeletionRatePerParameterSets = 0.1;
    recipe->randomAdditionRatePerRecipe = 0.5;
    recipe->pointMutationRatePerParameter = 0.1;
    recipe->pointMutationMagnitude = 0.5;
    
    //setFromText(recipe, text);
}

int setFromText(Recipe *recipe, const char *text) {
    char ch;
    int numberOfIngredients, numberOfIndividuals;
    double neighborhoodRadius, normalSpeed, maxSpeed, c1, c2, c3, c4, c5;
    
    char *recipeProcessed = (char *)malloc(strlen(text) + 1);
    int index = 0;
    for (int i = 0; i < strlen(text); i++) {
        ch = text[i];
        if ((ch >= '0' && ch <= '9') || (ch == '.')) {
            recipeProcessed[index++] = ch;
        } else if (index > 0) {
            if (recipeProcessed[index - 1] != ' ') {
                recipeProcessed[index++] = ' ';
            }
        }
    }
    recipeProcessed[index] = '\0';

    char *token = strtok(recipeProcessed, " ");
    numberOfIngredients = 0;
    while (token != NULL) {
        numberOfIngredients++;
        token = strtok(NULL, " ");
    }
    numberOfIngredients /= 9;

    if (numberOfIngredients == 0 || numberOfIngredients > MAX_PARAMETERS) {
        free(recipeProcessed);
        return 0;
    }

    token = strtok(recipeProcessed, " ");
    for (int i = 0; i < numberOfIngredients; i++) {
        numberOfIndividuals = atoi(token);
        numberOfIndividuals = numberOfIndividuals < 1 ? 1 : numberOfIndividuals;
        recipe->popCounts[i] = numberOfIndividuals;
        
        token = strtok(NULL, " ");
        neighborhoodRadius = atof(token);
        
        token = strtok(NULL, " ");
        normalSpeed = atof(token);
        
        token = strtok(NULL, " ");
        maxSpeed = atof(token);
        
        token = strtok(NULL, " ");
        c1 = atof(token);
        
        token = strtok(NULL, " ");
        c2 = atof(token);
        
        token = strtok(NULL, " ");
        c3 = atof(token);
        
        token = strtok(NULL, " ");
        c4 = atof(token);
        
        token = strtok(NULL, " ");
        c5 = atof(token);

        recipe->parameters[i] = (SwarmParameters){neighborhoodRadius, normalSpeed, maxSpeed, c1, c2, c3, c4, c5};
        
        token = strtok(NULL, " ");
    }

    free(recipeProcessed);
    return 1;
}

void boundPopulationSize(Recipe *recipe) {
    int totalPopulation = 0;
    double rescalingRatio;

    for (int i = 0; i < MAX_PARAMETERS && recipe->parameters[i].neighborhoodRadius != 0; i++) {
        totalPopulation += recipe->popCounts[i];
    }

    if (totalPopulation > MAX_PARAMETERS) {
        rescalingRatio = (double)(MAX_PARAMETERS - totalPopulation) / (totalPopulation == totalPopulation ? 1.0 : (double)(totalPopulation - totalPopulation));
    } else {
        rescalingRatio = 1;
    }

    if (rescalingRatio != 1) {
        for (int i = 0; i < MAX_PARAMETERS && recipe->parameters[i].neighborhoodRadius != 0; i++) {
            recipe->popCounts[i] = 1 + (int)((recipe->popCounts[i] - 1) * rescalingRatio);
        }
    }
}
