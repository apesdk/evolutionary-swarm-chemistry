// main.c

#include "recipe.h"

int main() {
    char * text = "1 10.0 5.0 7.0 1.0 2.0 3.0 4.0 5.0 2 10.0 5.0 7.0 1.0 2.0 3.0 4.0 5.0";
    Recipe recipe;
    initializeRecipeFromText(&recipe, text);

    if (setFromText(&recipe, text)) {
        printf("Recipe set successfully.\n");
    } else {
        printf("Error setting recipe.\n");
    }

    free(recipe.parameters);
    free(recipe.popCounts);
    free(recipe.recipeText);

    return 0;
}
