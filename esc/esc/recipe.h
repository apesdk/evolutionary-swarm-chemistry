// Recipe.h

#ifndef RECIPE_H
#define RECIPE_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define MAX_PARAMETERS 10000

typedef struct {
    double neighborhoodRadius;
    double normalSpeed;
    double maxSpeed;
    double c1;
    double c2;
    double c3;
    double c4;
    double c5;
} SwarmParameters;

typedef struct {
    SwarmParameters *parameters;
    int *popCounts;
    char *recipeText;
    double populationChangeMagnitude;
    double duplicationOrDeletionRatePerParameterSets;
    double randomAdditionRatePerRecipe;
    double pointMutationRatePerParameter;
    double pointMutationMagnitude;
} Recipe;

void initializeRecipeFromText(Recipe *recipe, const char *text);
int setFromText(Recipe *recipe, const char *text);
void boundPopulationSize(Recipe *recipe);

#endif // RECIPE_H
