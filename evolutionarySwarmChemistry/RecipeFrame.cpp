// RecipeFrame.cpp
//
// 2006-2015 (c) Copyright by Hiroki Sayama
//
// This file is part of "Evolutionary Swarm Chemistry Simulator",
// which is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// "Evolutionary Swarm Chemistry Simulator" is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with "Evolutionary Swarm Chemistry Simulator".  If not, see
// <http://www.gnu.org/licenses/>.
//
// Send any correspondences to:
//   Hiroki Sayama, D.Sc.
//   Director, Center for Collective Dynamics of Complex Systems
//   Associate Professor, Department of Systems Science and Industrial Engineering
//   Binghamton University, State University of New York
//   P.O. Box 6000, Binghamton, NY 13902-6000, USA
//   Tel: +1-607-777-3566
//   Email: sayama@binghamton.edu
//
// For more information about this software, see:
//   http://bingweb.binghamton.edu/~sayama/SwarmChemistry/

#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <algorithm>
#include <mutex>
//#include <gtk/gtk.h>
#include "SwarmPopulationSimulator.h"
#include "Recipe.h"
#include "SwarmPopulation.h"
#include "SwarmIndividual.h"

class RecipeFrame {
public:
    RecipeFrame(SwarmPopulationSimulator* par, const std::vector<SwarmIndividual*>& swarmInAnyOrder, int w, int h, std::vector<RecipeFrame*>& rcfs)
        : parentFrame(par), width(w), height(h), recipeFrames(rcfs) {
        parentFrame->displayedRecipe = this;
        recipe = new Recipe(swarmInAnyOrder);

        // Initialize GTK
        gtk_init(nullptr, nullptr);

        window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
        gtk_window_set_title(GTK_WINDOW(window), ("Recipe of Swarm #" + std::to_string(parentFrame->frameNumber + 1)).c_str());
        gtk_window_set_default_size(GTK_WINDOW(window), 600, 240);

        g_signal_connect(window, "destroy", G_CALLBACK(on_window_destroy), this);

        // Layout setup
        GtkWidget* main_box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5);
        gtk_container_add(GTK_CONTAINER(window), main_box);

        // Right panel for image
        GtkWidget* right_panel = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
        gtk_box_pack_start(GTK_BOX(main_box), right_panel, FALSE, FALSE, 0);
        GtkWidget* label = gtk_label_new("Screen shot");
        gtk_box_pack_start(GTK_BOX(right_panel), label, FALSE, FALSE, 0);

        image = gtk_image_new();
        gtk_box_pack_start(GTK_BOX(right_panel), image, TRUE, TRUE, 0);

        // Left panel for recipe text and button
        GtkWidget* left_panel = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
        gtk_box_pack_start(GTK_BOX(main_box), left_panel, TRUE, TRUE, 0);

        GtkWidget* recipe_format_label = gtk_label_new("Format: # of agents * (R, Vn, Vm, c1, c2, c3, c4, c5)");
        gtk_box_pack_start(GTK_BOX(left_panel), recipe_format_label, FALSE, FALSE, 0);

        recipe_box = gtk_text_view_new();
        gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(recipe_box), GTK_WRAP_WORD);
        gtk_text_view_set_editable(GTK_TEXT_VIEW(recipe_box), TRUE);
        gtk_text_buffer_set_text(gtk_text_view_get_buffer(GTK_TEXT_VIEW(recipe_box)), recipe->recipeText.c_str(), -1);
        gtk_box_pack_start(GTK_BOX(left_panel), recipe_box, TRUE, TRUE, 0);

        apply_button = gtk_button_new_with_label("Apply edits");
        g_signal_connect(apply_button, "clicked", G_CALLBACK(on_apply_button_clicked), this);
        gtk_box_pack_start(GTK_BOX(left_panel), apply_button, FALSE, FALSE, 0);

        // Show all widgets
        gtk_widget_show_all(window);

        // Load initial image
        putImage(parentFrame->im);
    }

    ~RecipeFrame() {
        delete recipe;
    }

    void putImage(GdkPixbuf* pixbuf) {
        gtk_image_set_from_pixbuf(GTK_IMAGE(image), pixbuf);
    }

    void orphanize() {
        gtk_window_set_title(GTK_WINDOW(window), "Orphaned recipe");
        putImage(parentFrame->im);
        gtk_text_view_set_editable(GTK_TEXT_VIEW(recipe_box), FALSE);
        gtk_widget_set_sensitive(apply_button, FALSE);
    }

private:
    static void on_window_destroy(GtkWidget* widget, gpointer data) {
        RecipeFrame* self = static_cast<RecipeFrame*>(data);
        std::lock_guard<std::mutex> lock(self->mutex);
        auto it = std::find(self->recipeFrames.begin(), self->recipeFrames.end(), self);
        if (it != self->recipeFrames.end()) {
            self->recipeFrames.erase(it);
            self->parentFrame->displayedRecipe = nullptr;
        }
        delete self;
    }

    static void on_apply_button_clicked(GtkWidget* widget, gpointer data) {
        RecipeFrame* self = static_cast<RecipeFrame*>(data);

        GtkTextBuffer* buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(self->recipe_box));
        GtkTextIter start, end;
        gtk_text_buffer_get_bounds(buffer, &start, &end);
        gchar* text = gtk_text_buffer_get_text(buffer, &start, &end, FALSE);

        self->recipe->setFromText(text);
        gtk_text_buffer_set_text(buffer, self->recipe->recipeText.c_str(), -1);

        if (self->recipe->recipeText[0] == '*') {
            gtk_widget_modify_base(self->recipe_box, GTK_STATE_NORMAL, gdk_color_parse("yellow"));
        } else {
            gtk_widget_modify_base(self->recipe_box, GTK_STATE_NORMAL, gdk_color_parse("white"));
            SwarmPopulation* newSwarmPop = new SwarmPopulation(self->recipe->createPopulation(self->width, self->height), "Created from a given recipe");
            self->parentFrame->replacePopulationWith(newSwarmPop);
            gdk_window_clear(gtk_widget_get_window(self->image));
            gtk_widget_queue_draw(self->image);
        }

        g_free(text);
    }

    SwarmPopulationSimulator* parentFrame;
    Recipe* recipe;
    int width, height;
    std::vector<RecipeFrame*>& recipeFrames;
    GtkWidget* window;
    GtkWidget* image;
    GtkWidget* recipe_box;
    GtkWidget* apply_button;
    std::mutex mutex;
};

#endif // RECIPEFRAME_H
