#ifndef RECIPE_H
#define RECIPE_H

#include <vector>
#include <string>
#include <sstream>
#include "SwarmParameters.h"
#include "SwarmIndividual.h"

class Recipe {
public:
    std::vector<SwarmParameters> parameters;
    std::vector<int> popCounts;
    std::string recipeText;

    double populationChangeMagnitude = 0.8;
    double duplicationOrDeletionRatePerParameterSets = 0.1;
    double randomAdditionRatePerRecipe = 0.5; // increased from 0.1 to 0.5
    double pointMutationRatePerParameter = 0.1;
    double pointMutationMagnitude = 0.5;

    Recipe(const std::string& text);
    Recipe(const std::vector<SwarmIndividual>& sol);

    bool setFromText(const std::string& text);
    void boundPopulationSize();
    void setFromPopulation(const std::vector<SwarmIndividual>& sol);
    std::vector<SwarmIndividual> createPopulation(int width, int height);
    SwarmParameters randomlyPickParameters();
    Recipe mutate();

private:
    void setRecipeText();
    double shorten(double d) const;
};

#endif
