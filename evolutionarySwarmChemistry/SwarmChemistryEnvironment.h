// SwarmChemistryEnvironment.cpp
//
// 2006-2015 (c) Copyright by Hiroki Sayama
//
// This file is part of "Evolutionary Swarm Chemistry Simulator",
// which is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// "Evolutionary Swarm Chemistry Simulator" is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with "Evolutionary Swarm Chemistry Simulator".  If not, see
// <http://www.gnu.org/licenses/>.
//
// Send any correspondences to:
//   Hiroki Sayama, D.Sc.
//   Director, Center for Collective Dynamics of Complex Systems
//   Associate Professor, Department of Systems Science and Industrial Engineering
//   Binghamton University, State University of New York
//   P.O. Box 6000, Binghamton, NY 13902-6000, USA
//   Tel: +1-607-777-3566
//   Email: sayama@binghamton.edu
//
// For more information about this software, see:
//   http://bingweb.binghamton.edu/~sayama/SwarmChemistry/

#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <cstdlib>
#include <ctime>
//#include <SFML/Graphics.hpp>
#include "SwarmPopulation.h"
#include "SwarmPopulationSimulator.h"
#include "RecipeFrame.h"


typedef struct {
    int isSelected;
    bool notYetNoticed;
} RecipeFrame;

typedef struct {
    int numberOfIndividualsMax;
} SwarmParameters;

typedef struct {
    int population;
    char* title;
} SwarmPopulation;

typedef struct {
    int size;
    SwarmParameters* parameters;
    int* popCounts;
} Recipe;

typedef struct {
    int isSelected;
    bool notYetNoticed;
    int displayedRecipe;
} SwarmPopulationSimulator;

typedef struct {
    int defaultFrameSize;
    int initialSpaceSize;
    int numberOfFrames;
    int showroomW;
    int showroomH;
    int maxFrames;
    bool resettingRequest;
    bool numberOfFramesChangeRequest;
    bool applicationRunning;
    bool thisIsApplet;
    int numberOfSeeds;
    bool givenWithSpecificRecipe;
    char* givenRecipeText;
    SwarmPopulation* populations;
    SwarmPopulationSimulator* sample;
    SwarmPopulationSimulator* selectedSample[2];
    RecipeFrame* recipeFrames;
    long previousTime;
    long currentTime;
    long milliSecPerFrame;
    double populationChangeMagnitude;
    double duplicationOrDeletionRatePerParameterSets;
    double randomAdditionRatePerRecipe;
    double pointMutationRatePerParameter;
    double pointMutationMagnitude;
    char* initcond;
} SwarmChemistryEnvironment;


class SwarmChemistryEnvironment {
public:
    SwarmChemistryEnvironment(bool app, const std::string& recipeText) {
        thisIsApplet = app;
        givenWithSpecificRecipe = true;
        givenRecipeText = recipeText;
        initcond = "designed";
        numberOfFrames = 1;
        numberOfSeeds = 1;
        constructControlFrame();
        determineFrameArrangement();
        createPopulations();
        run();
    }

    SwarmChemistryEnvironment(bool app, int num) {
        thisIsApplet = app;
        givenWithSpecificRecipe = false;
        numberOfSeeds = num;
        initcond = "random";
        numberOfFrames = 1;
        constructControlFrame();
        determineFrameArrangement();
        createPopulations();
        run();
    }

    SwarmChemistryEnvironment(bool app, int num, bool given, const std::string& recipeText) {
        thisIsApplet = app;
        givenWithSpecificRecipe = given;
        givenRecipeText = recipeText;
        initcond = given ? "designed" : "random";
        numberOfFrames = 1;
        numberOfSeeds = num;
        constructControlFrame();
        determineFrameArrangement();
        createPopulations();
        run();
    }

    void run() {
        applicationRunning = true;
        while (applicationRunning) {
            int timestep = 0;
            int endtime = 30000;
            int imageoutputinterval = 10;

            createFrames();

            int howManySelected = 0;
            resettingRequest = false;
            numberOfFramesChangeRequest = false;

            while (howManySelected < 2 && !resettingRequest && !numberOfFramesChangeRequest) {
                for (int i = 0; i < numberOfFrames; i++) {
                    if (sample[i]->isDisplayable()) {
                        if (!pausing) {
                            sample[i]->simulateSwarmBehavior(compfunc);
                            sample[i]->updateStates();
                        }
                        timestep++;
                        if (timestep % imageoutputinterval == 0) {
                            sample[i]->displayStates();
                            sample[i]->saveImage(initcond, timestep);
                        }
                        if (timestep >= endtime) {
                            std::cout << "Simulation finished" << std::endl;
                            std::exit(0);
                        }

                        elapsedTime++;

                        if (isUnderPerturbation) {
                            if (elapsedTime >= perturbationLength) {
                                isUnderPerturbation = false;
                                elapsedTime = 0;
                                compfunc = "majority-relative";
                            }
                        } else {
                            if (elapsedTime >= steadyEnvironmentLength) {
                                isUnderPerturbation = true;
                                elapsedTime = 0;
                                double r = static_cast<double>(std::rand()) / RAND_MAX;
                                if (r < 0.25) compfunc = "leftfaster";
                                else if (r < 0.5) compfunc = "rightfaster";
                                else if (r < 0.75) compfunc = "leftslower";
                                else compfunc = "rightslower";
                            }
                        }

                        if (sample[i]->isSelected && sample[i]->notYetNoticed) {
                            sample[i]->notYetNoticed = false;
                            selectedSample[howManySelected] = sample[i];
                            howManySelected++;
                        } else if (!sample[i]->isSelected && !sample[i]->notYetNoticed) {
                            sample[i]->notYetNoticed = true;
                            howManySelected--;
                        }
                    } else {
                        if (sample[i]->isSelected && !sample[i]->notYetNoticed) {
                            sample[i]->isSelected = false;
                            sample[i]->notYetNoticed = true;
                            howManySelected--;
                        }
                    }
                }
            }

            disposeFrames();

            if (resettingRequest) createPopulations();
            else if (numberOfFramesChangeRequest) {
                int n = std::stoi(numberOfFramesText.getText());

                std::vector<SwarmPopulation> newPopulations(n);
                for (int i = 0; i < n; i++) {
                    if (i < numberOfFrames) newPopulations[i] = populations[i];
                    else newPopulations[i] = SwarmPopulation(static_cast<int>(std::rand() % SwarmParameters::numberOfIndividualsMax) + 1, "Randomly generated");
                }
                numberOfFrames = n;
                populations = newPopulations;
                determineFrameArrangement();
            } else {
                for (int i = 0; i < numberOfFrames; i++) {
                    if (i == 0)
                        populations[i] = SwarmPopulation(selectedSample[0]->population, "Previously selected");
                    else if (i == 1 && selectedSample[0] != selectedSample[1])
                        populations[i] = SwarmPopulation(selectedSample[1]->population, "Previously selected");
                    else if (i == numberOfFrames - 1 && numberOfFrames > 3) {
                        populations[i] = SwarmPopulation(static_cast<int>(std::rand() % SwarmParameters::numberOfIndividualsMax) + 1, "Randomly generated");
                    } else {
                        if (selectedSample[0] == selectedSample[1]) {
                            populations[i] = SwarmPopulation(selectedSample[0]->population, "Perturbed");
                            populations[i].perturb(populationChangeMagnitude, initialSpaceSize);
                        } else {
                            populations[i] = SwarmPopulation(selectedSample[0]->population, selectedSample[1]->population, static_cast<double>(std::rand()) / RAND_MAX * 0.6 + 0.2, "Mixed");
                        }

                        if (mutation) {
                            bool mutated = false;

                            Recipe tempRecipe(populations[i].population);
                            int numberOfIngredients = tempRecipe.parameters.size();

                            for (int j = 0; j < numberOfIngredients; j++) {
                                if (static_cast<double>(std::rand()) / RAND_MAX < duplicationOrDeletionRatePerParameterSets) {
                                    if (static_cast<double>(std::rand()) / RAND_MAX < 0.5) {
                                        mutated = true;
                                        tempRecipe.parameters.insert(tempRecipe.parameters.begin() + j + 1, tempRecipe.parameters[j]);
                                        tempRecipe.popCounts.insert(tempRecipe.popCounts.begin() + j + 1, tempRecipe.popCounts[j]);
                                        numberOfIngredients++;
                                        j++;
                                    } else {
                                        if (numberOfIngredients > 1) {
                                            mutated = true;
                                            tempRecipe.parameters.erase(tempRecipe.parameters.begin() + j);
                                            tempRecipe.popCounts.erase(tempRecipe.popCounts.begin() + j);
                                            numberOfIngredients--;
                                            j--;
                                        }
                                    }
                                }
                            }

                            if (static_cast<double>(std::rand()) / RAND_MAX < randomAdditionRatePerRecipe) {
                                mutated = true;
                                tempRecipe.parameters.push_back(SwarmParameters());
                                tempRecipe.popCounts.push_back(static_cast<int>(std::rand() % SwarmParameters::numberOfIndividualsMax * 0.5) + 1);
                            }

                            for (int j = 0; j < numberOfIngredients; j++) {
                                SwarmParameters tempParam(tempRecipe.parameters[j]);
                                tempParam.inducePointMutations(pointMutationRatePerParameter, pointMutationMagnitude);
                                if (!(tempRecipe.parameters[j] == tempParam)) {
                                    mutated = true;
                                    tempRecipe.parameters[j] = tempParam;
                                }
                            }

                            tempRecipe.boundPopulationSize();
                            populations[i].population = tempRecipe.createPopulation(defaultFrameSize, defaultFrameSize);

                            if (mutated) {
                                populations[i].title += " & mutated";
                            }
                        }
                    }
                }
            }
        }
    }

private:
    void constructControlFrame() {
        // Implementation of control frame construction using a GUI library
        // Example: Use SFML or Qt for GUI elements, such as buttons and text fields
    }

    void determineFrameArrangement() {
        showroomH = showroomW = 1;
        defaultFrameSize = 400;
    }

    void createPopulations() {
        populations.resize(numberOfFrames);
        for (int i = 0; i < numberOfFrames; i++) {
            if (givenWithSpecificRecipe) {
                givenWithSpecificRecipe = false;
                populations[i] = SwarmPopulation(givenRecipeText, "Created from a given recipe");
            } else {
                populations[i] = SwarmPopulation(numberOfSeeds, "Randomly generated");
            }
        }
    }

    void createFrames() {
        sample.resize(numberOfFrames);

        for (int i = 0; i < numberOfFrames; i++) {
            int x = i % showroomW;
            int y = (i - x) / showroomW;
            sample[i] = std::make_shared<SwarmPopulationSimulator>(defaultFrameSize, initialSpaceSize, populations[i], populations, i, tracking, mouseEffect, recipeFrames);
            simulatorDimension = sample[i]->getSize();
            sample[i]->setPosition(simulatorDimension.width * x + (screenDimension.width - simulatorDimension.width * showroomW) / 2, simulatorDimension.height * y);
        }
    }

    void disposeFrames() {
        for (int i = 0; i < numberOfFrames; i++) {
            if (sample[i]->displayedRecipe) {
                sample[i]->displayedRecipe->orphanize();
            }
            sample[i]->dispose();
        }
    }

private:
    bool thisIsApplet;
    bool givenWithSpecificRecipe;
    std::string givenRecipeText;
    std::string initcond;
    int numberOfFrames;
    int numberOfSeeds;
    std::vector<SwarmPopulation> populations;
    std::vector<std::shared_ptr<SwarmPopulationSimulator>> sample;
    std::shared_ptr<SwarmPopulationSimulator> selectedSample[2];
    std::vector<std::shared_ptr<RecipeFrame>> recipeFrames;
    sf::RenderWindow controlFrame;
    sf::Vector2u screenDimension;
    sf::Vector2u controlFrameDimension;
    sf::Vector2u simulatorDimension;
    int showroomW, showroomH, defaultFrameSize;
    int initialSpaceSize = 2000;
    bool pausing = false;
    bool mutation = false;
    bool mouseEffect = false;
    bool resettingRequest = false;
    bool numberOfFramesChangeRequest = false;
    bool applicationRunning = false;
    long previousTime = 0;
    long currentTime = 0;
    long milliSecPerFrame = 70;
    double populationChangeMagnitude = 0.8;
    double duplicationOrDeletionRatePerParameterSets = 0.1;
    double randomAdditionRatePerRecipe = 0.1;
    double pointMutationRatePerParameter = 0.1;
    double pointMutationMagnitude = 0.5;
    int steadyEnvironmentLength = 1950;
    int perturbationLength = 50;
    bool isUnderPerturbation = false;
    int elapsedTime = 0;
    std::string compfunc = "majority-relative";
};

int main(int argc, char* argv[]) {
    std::srand(std::time(0));
    bool isApplet = false;
    std::string recipeText = "";

    if (argc > 1) {
        isApplet = std::string(argv[1]) == "applet";
        if (argc > 2) {
            recipeText = argv[2];
        }
    }

    SwarmChemistryEnvironment environment(isApplet, recipeText);
    return 0;
}
