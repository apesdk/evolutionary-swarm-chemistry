#include "Recipe.h"
#include <algorithm>
#include <cstdlib>
#include <cmath>
#include <stdexcept>
#include <iterator>

Recipe::Recipe(const std::string& text) {
    setFromText(text);
}

Recipe::Recipe(const std::vector<SwarmIndividual>& sol) {
    setFromPopulation(sol);
}

bool Recipe::setFromText(const std::string& text) {
    char ch;
    int numberOfIngredients, numberOfIndividuals;
    double neighborhoodRadius, normalSpeed, maxSpeed, c1, c2, c3, c4, c5;

    std::stringstream recipeProcessed;
    for (char ch : text) {
        if ((ch >= '0' && ch <= '9') || (ch == '.')) {
            recipeProcessed << ch;
        } else if (recipeProcessed.tellp() > 0) {
            if (recipeProcessed.str().back() != ' ') {
                recipeProcessed << ' ';
            }
        }
    }

    std::string recipeStr = recipeProcessed.str();
    std::istringstream st(recipeStr);

    if (std::count(std::istream_iterator<std::string>(st), std::istream_iterator<std::string>(), "") % 9 != 0) {
        recipeText = "*** Formatting error ***\n" + text;
        parameters.clear();
        popCounts.clear();
        return false;
    }

    st.clear();
    st.str(recipeStr);

    numberOfIngredients = std::count(std::istream_iterator<std::string>(st), std::istream_iterator<std::string>(), "") / 9;
    if (numberOfIngredients == 0) {
        recipeText = "*** No ingredients ***\n" + text;
        parameters.clear();
        popCounts.clear();
        return false;
    }
    if (numberOfIngredients > SwarmParameters::numberOfIndividualsMax)
        numberOfIngredients = SwarmParameters::numberOfIndividualsMax;

    parameters.clear();
    popCounts.clear();

    try {
        for (int i = 0; i < numberOfIngredients; ++i) {
            st >> numberOfIndividuals;
            if (numberOfIndividuals < 1) numberOfIndividuals = 1;
            for (int j = 0; j < 8; ++j) st.ignore(std::numeric_limits<std::streamsize>::max(), ' ');
        }

        st.clear();
        st.str(recipeStr);

        for (int i = 0; i < numberOfIngredients; ++i) {
            st >> numberOfIndividuals;
            if (numberOfIndividuals < 1) numberOfIndividuals = 1;
            st >> neighborhoodRadius >> normalSpeed >> maxSpeed >> c1 >> c2 >> c3 >> c4 >> c5;
            parameters.emplace_back(neighborhoodRadius, normalSpeed, maxSpeed, c1, c2, c3, c4, c5);
            popCounts.push_back(numberOfIndividuals);
        }
    } catch (const std::exception&) {
        recipeText = "*** Formatting error ***\n" + text;
        parameters.clear();
        popCounts.clear();
        return false;
    }

    return true;
}

void Recipe::boundPopulationSize() {
    int totalPopulation = 0;
    double rescalingRatio;

    int numberOfIngredients = parameters.size();

    for (int i = 0; i < numberOfIngredients; ++i)
        totalPopulation += popCounts[i];

    if (totalPopulation > SwarmParameters::numberOfIndividualsMax)
        rescalingRatio = static_cast<double>(SwarmParameters::numberOfIndividualsMax - numberOfIngredients) / (totalPopulation == numberOfIngredients ? 1.0 : static_cast<double>(totalPopulation - numberOfIngredients));
    else
        rescalingRatio = 1;

    if (rescalingRatio != 1)
        for (int i = 0; i < numberOfIngredients; ++i)
            popCounts[i] = 1 + static_cast<int>(std::floor(static_cast<double>(popCounts[i] - 1) * rescalingRatio));
}

void Recipe::setFromPopulation(const std::vector<SwarmIndividual>& sol) {
    parameters.clear();
    popCounts.clear();

    for (const auto& individual : sol) {
        const auto& tempParam = individual.genome;

        bool alreadyInParameters = false;
        for (size_t j = 0; j < parameters.size(); ++j) {
            if (parameters[j] == tempParam) {
                alreadyInParameters = true;
                ++popCounts[j];
                break;
            }
        }
        if (!alreadyInParameters) {
            parameters.push_back(tempParam);
            popCounts.push_back(1);
        }
    }

    setRecipeText();
}

void Recipe::setRecipeText() {
    recipeText.clear();

    for (size_t i = 0; i < parameters.size(); ++i) {
        const auto& tempParam = parameters[i];
        recipeText += std::to_string(popCounts[i]) + " * ("
            + std::to_string(shorten(tempParam.neighborhoodRadius)) + ", "
            + std::to_string(shorten(tempParam.normalSpeed)) + ", "
            + std::to_string(shorten(tempParam.maxSpeed)) + ", "
            + std::to_string(shorten(tempParam.c1)) + ", "
            + std::to_string(shorten(tempParam.c2)) + ", "
            + std::to_string(shorten(tempParam.c3)) + ", "
            + std::to_string(shorten(tempParam.c4)) + ", "
            + std::to_string(shorten(tempParam.c5)) + ")\n";
    }
}

double Recipe::shorten(double d) const {
    return std::round(d * 100.0) / 100.0;
}

std::vector<SwarmIndividual> Recipe::createPopulation(int width, int height) {
    std::vector<SwarmIndividual> newPopulation;
    if (parameters.empty()) return newPopulation;

    // Example population creation logic, needs to be adapted to actual logic
    for (const auto& param : parameters) {
        for (int count = 0; count < popCounts[&param - &parameters[0]]; ++count) {
            newPopulation.emplace_back(150, 150, std::rand() % 10 - 5, std::rand() % 10 - 5, param);
        }
    }
    return newPopulation;
}

SwarmParameters Recipe::randomlyPickParameters() {
    int totalPopulation = 0;
    int numberOfIngredients = parameters.size();

    for (int count : popCounts)
        totalPopulation += count;

    int r = std::rand() % totalPopulation;

    int j = 0;
    for (int i = 0; i < numberOfIngredients; ++i) {
        if (r >= j && r < j + popCounts[i]) return parameters[i];
        else j += popCounts[i];
    }

    return parameters[0];
}

Recipe Recipe::mutate() {
    setRecipeText();
    Recipe tempRecipe(recipeText);

    int numberOfIngredients = tempRecipe.parameters.size();

    // Insertions, duplications, and deletions
    for (int j = 0; j < numberOfIngredients; ++j) {
        if (static_cast<double>(std::rand()) / RAND_MAX < duplicationOrDeletionRatePerParameterSets) {
            if (static_cast<double>(std::rand()) / RAND_MAX < 0.5) { // Duplication
                tempRecipe.parameters.insert(tempRecipe.parameters.begin() + j + 1, tempRecipe.parameters[j]);
                tempRecipe.popCounts.insert(tempRecipe.popCounts.begin() + j + 1, tempRecipe.popCounts[j]);
                ++numberOfIngredients;
                ++j;
            } else { // Deletion
                if (numberOfIngredients > 1) {
                    tempRecipe.parameters.erase(tempRecipe.parameters.begin() + j);
                    tempRecipe.popCounts.erase(tempRecipe.popCounts.begin() + j);
                    --numberOfIngredients;
                    --j;
                }
            }
        }
    }

    if (static_cast<double>(std::rand()) / RAND_MAX < randomAdditionRatePerRecipe) { // Addition
        tempRecipe.parameters.emplace_back();
        tempRecipe.popCounts.push_back(static_cast<int>(std::rand() % static_cast<int>(SwarmParameters::numberOfIndividualsMax * 0.5) + 1));
    }

    // Then Point Mutations
    for (int j = 0; j < numberOfIngredients; ++j) {
        SwarmParameters tempParam = tempRecipe.parameters[j];
        tempParam.inducePointMutations(pointMutationRatePerParameter, pointMutationMagnitude);
        if (!(tempRecipe.parameters[j] == tempParam)) {
            tempRecipe.parameters[j] = tempParam;
        }
    }

    return tempRecipe;
}
