#include "SwarmIndividual.h"

SwarmIndividual::SwarmIndividual()
    : x(0), y(0), dx(0), dy(0), genome() {}

SwarmIndividual::SwarmIndividual(double xx, double yy, double dxx, double dyy, const SwarmParameters& g)
    : x(xx), y(yy), dx(dxx), dy(dyy), genome(g) {}
