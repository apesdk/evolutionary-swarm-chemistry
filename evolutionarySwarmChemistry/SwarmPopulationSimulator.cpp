#include "SwarmPopulationSimulator.h"

SwarmPopulationSimulator::SwarmPopulationSimulator(int frameSize, int spaceSize, SwarmPopulation sol, 
                                                   std::vector<SwarmPopulation> solList, int num, 
                                                   bool tr, bool mo, std::vector<RecipeFrame>& rcfs) 
    : sf::RenderWindow(sf::VideoMode(frameSize, frameSize), "Swarm #" + std::to_string(num + 1) + ": " + sol.title), 
      originalPopulationList(solList), recipeFrames(rcfs), tracking(tr), mouseEffect(mo), 
      frameNumber(num), population(sol), displayedRecipe(nullptr), isMouseIn(false), isSelected(false), 
      notYetNoticed(true), currentMidX(0), currentMidY(0), currentScalingFactor(0) {
    
    width = height = frameSize;
    originalWidth = originalHeight = spaceSize;
    
    img.create(width, height);

    clearImage();

    for (auto& individual : sol.population) {
        addSwarm(individual);
    }

    displayStates();
}

void SwarmPopulationSimulator::clearImage() {
    img.clear(sf::Color::White);
    displayStates();
}

void SwarmPopulationSimulator::addSwarm(SwarmIndividual b) {
    swarmInBirthOrder.push_back(b);

    auto itX = std::lower_bound(swarmInXOrder.begin(), swarmInXOrder.end(), b, [](const SwarmIndividual& a, const SwarmIndividual& b) {
        return a.x < b.x;
    });
    swarmInXOrder.insert(itX, b);

    auto itY = std::lower_bound(swarmInYOrder.begin(), swarmInYOrder.end(), b, [](const SwarmIndividual& a, const SwarmIndividual& b) {
        return a.y < b.y;
    });
    swarmInYOrder.insert(itY, b);
}

void SwarmPopulationSimulator::replacePopulationWith(SwarmPopulation newpop) {
    setTitle("Swarm #" + std::to_string(frameNumber + 1) + ": " + newpop.title);

    population = newpop;
    originalPopulationList[frameNumber] = population;

    currentMidX = 0;
    currentMidY = 0;
    currentScalingFactor = 0;
    swarmDiameter = swarmRadius * 2;
    swarmInBirthOrder.clear();
    swarmInXOrder.clear();
    swarmInYOrder.clear();

    for (auto& individual : newpop.population) {
        addSwarm(individual);
    }

    displayStates();
}

void SwarmPopulationSimulator::resetRanksInSwarm() {
    for (int i = 0; i < swarmInXOrder.size(); ++i) {
        if (swarmInXOrder[i].rankInXOrder != -1) {
            swarmInXOrder[i].rankInXOrder = i;
        } else {
            swarmInXOrder.erase(swarmInXOrder.begin() + i--);
        }
    }

    for (int i = 0; i < swarmInYOrder.size(); ++i) {
        if (swarmInYOrder[i].rankInYOrder != -1) {
            swarmInYOrder[i].rankInYOrder = i;
        } else {
            swarmInYOrder.erase(swarmInYOrder.begin() + i--);
        }
    }
}

bool SwarmPopulationSimulator::losing(SwarmIndividual defender, SwarmIndividual attacker, const std::string& compfunc) {
    // Implement competition function logic
    return false;
}

std::vector<SwarmIndividual> SwarmPopulationSimulator::neighborsOf(SwarmIndividual tempSwarm, double radius) {
    std::vector<SwarmIndividual> ngbs;

    double tempX = tempSwarm.x;
    double tempY = tempSwarm.y;
    double neighborhoodRadiusSquared = radius * radius;

    SwarmIndividual tempSwarm2;

    int numberOfSwarm = swarmInBirthOrder.size();

    // Detecting neighbors using sorted lists

    double minX = tempX - radius;
    double maxX = tempX + radius;
    double minY = tempY - radius;
    double maxY = tempY + radius;
    int minRankInXOrder = tempSwarm.rankInXOrder;
    int maxRankInXOrder = tempSwarm.rankInXOrder;
    int minRankInYOrder = tempSwarm.rankInYOrder;
    int maxRankInYOrder = tempSwarm.rankInYOrder;

    for (int j = tempSwarm.rankInXOrder - 1; j >= 0; --j) {
        if (swarmInXOrder[j].x >= minX) {
            minRankInXOrder = j;
        } else {
            break;
        }
    }
    for (int j = tempSwarm.rankInXOrder + 1; j < numberOfSwarm; ++j) {
        if (swarmInXOrder[j].x <= maxX) {
            maxRankInXOrder = j;
        } else {
            break;
        }
    }
    for (int j = tempSwarm.rankInYOrder - 1; j >= 0; --j) {
        if (swarmInYOrder[j].y >= minY) {
            minRankInYOrder = j;
        } else {
            break;
        }
    }
    for (int j = tempSwarm.rankInYOrder + 1; j < numberOfSwarm; ++j) {
        if (swarmInYOrder[j].y <= maxY) {
            maxRankInYOrder = j;
        } else {
            break;
        }
    }

    if (maxRankInXOrder - minRankInXOrder < maxRankInYOrder - minRankInYOrder) {
        for (int j = minRankInXOrder; j <= maxRankInXOrder; ++j) {
            tempSwarm2 = swarmInXOrder[j];
            if (tempSwarm != tempSwarm2) {
                if (tempSwarm2.rankInYOrder >= minRankInYOrder && tempSwarm2.rankInYOrder <= maxRankInYOrder) {
                    if ((tempSwarm2.x - tempSwarm.x) * (tempSwarm2.x - tempSwarm.x) +
                        (tempSwarm2.y - tempSwarm.y) * (tempSwarm2.y - tempSwarm.y) < neighborhoodRadiusSquared) {
                        ngbs.push_back(tempSwarm2);
                    }
                }
            }
        }
    } else {
        for (int j = minRankInYOrder; j <= maxRankInYOrder; ++j) {
            tempSwarm2 = swarmInYOrder[j];
            if (tempSwarm != tempSwarm2) {
                if (tempSwarm2.rankInXOrder >= minRankInXOrder && tempSwarm2.rankInXOrder <= maxRankInXOrder) {
                    if ((tempSwarm2.x - tempSwarm.x) * (tempSwarm2.x - tempSwarm.x) +
                        (tempSwarm2.y - tempSwarm.y) * (tempSwarm2.y - tempSwarm.y) < neighborhoodRadiusSquared) {
                        ngbs.push_back(tempSwarm2);
                    }
                }
            }
        }
    }

    return ngbs;
}

void SwarmPopulationSimulator::simulateSwarmBehavior(const std::string& compfunc) {
    SwarmIndividual tempSwarm, tempSwarm2;
    SwarmParameters param;

    double tempX, tempY, tempX2, tempY2, tempDX, tempDY;
    double localCenterX, localCenterY, localDX, localDY, tempAx, tempAy, d;
    int n;

    std::vector<SwarmIndividual> neighbors;

    int numberOfSwarm = swarmInBirthOrder.size();

    SwarmIndividual mouseCursor;

    if (mouseEffect && isMouseIn) {
        mouseCursor.x = ((double)(mouseX - width / 2)) / currentScalingFactor + currentMidX;
        mouseCursor.y = ((double)(mouseY - height / 2)) / currentScalingFactor + currentMidY;
    }

    for (int i = 0; i < numberOfSwarm; ++i) {
        tempSwarm = swarmInBirthOrder[i];
        param = tempSwarm.genome;
        tempX = tempSwarm.x;
        tempY = tempSwarm.y;

        // Simulating recipe transmission by collision

        double minRSquared = 10.0 * 10.0;
        double tempRSquared;
        SwarmIndividual* nearest = nullptr;

        neighbors = neighborsOf(tempSwarm, 10);
        n = neighbors.size();
        for (int j = 0; j < n; ++j) {
            tempSwarm2 = neighbors[j];
            if (tempSwarm2.recipe != nullptr) {
                tempRSquared = (tempX - tempSwarm2.x) * (tempX - tempSwarm2.x) + (tempY - tempSwarm2.y) * (tempY - tempSwarm2.y);
                if (tempRSquared < minRSquared) {
                    minRSquared = tempRSquared;
                    nearest = &tempSwarm2;
                }
            }
        }

        if (nearest != nullptr) {
            if (tempSwarm.recipe != nearest->recipe) {
                if (tempSwarm.recipe == nullptr || losing(tempSwarm, *nearest, compfunc)) {
                    if (static_cast<double>(std::rand()) / RAND_MAX < mutationRateAtTransmission)
                        tempSwarm.recipe = nearest->recipe->mutate();
                    else
                        tempSwarm.recipe = nearest->recipe;
                    tempSwarm.genome = tempSwarm.recipe->randomlyPickParameters();
                }
            }
        }

        // Finding neighbors within interaction range

        neighbors = neighborsOf(tempSwarm, param.neighborhoodRadius);

        if (mouseEffect && isMouseIn) {
            if ((mouseCursor.x - tempX) * (mouseCursor.x - tempX) +
                (mouseCursor.y - tempY) * (mouseCursor.y - tempY) < param.neighborhoodRadius * param.neighborhoodRadius) {
                for (int j = 0; j < weightOfMouseCursor; ++j) {
                    neighbors.push_back(mouseCursor);
                }
            }
        }

        // Simulating the behavior of swarm agents

        n = neighbors.size();

        if (n == 0) {
            tempAx = static_cast<double>(std::rand()) / RAND_MAX - 0.5;
            tempAy = static_cast<double>(std::rand()) / RAND_MAX - 0.5;
        } else {
            localCenterX = localCenterY = 0;
            localDX = localDY = 0;
            for (int j = 0; j < n; ++j) {
                tempSwarm2 = neighbors[j];
                localCenterX += tempSwarm2.x;
                localCenterY += tempSwarm2.y;
                localDX += tempSwarm2.dx;
                localDY += tempSwarm2.dy;
            }

            localCenterX /= n;
            localCenterY /= n;
            localDX /= n;
            localDY /= n;

            if (tempSwarm.recipe != nullptr) {
                if (static_cast<double>(std::rand()) / RAND_MAX < 0.005) {
                    tempSwarm.genome = tempSwarm.recipe->randomlyPickParameters();
                    param = tempSwarm.genome;
                }
            }

            tempAx = tempAy = 0;

            tempAx += (localCenterX - tempX) * param.c1;
            tempAy += (localCenterY - tempY) * param.c1;

            tempAx += (localDX - tempSwarm.dx) * param.c2;
            tempAy += (localDY - tempSwarm.dy) * param.c2;

            for (int j = 0; j < n; ++j) {
                tempSwarm2 = neighbors[j];
                tempX2 = tempSwarm2.x;
                tempY2 = tempSwarm2.y;
                d = (tempX - tempX2) * (tempX - tempX2) + (tempY - tempY2) * (tempY - tempY2);
                if (d == 0) d = 0.001;
                tempAx += (tempX - tempX2) / d * param.c3;
                tempAy += (tempY - tempY2) / d * param.c3;
            }

            if (static_cast<double>(std::rand()) / RAND_MAX < param.c4) {
                tempAx += static_cast<double>(std::rand()) / RAND_MAX * 10 - 5;
                tempAy += static_cast<double>(std::rand()) / RAND_MAX * 10 - 5;
            }
        }

        tempSwarm.accelerate(tempAx, tempAy, param.maxSpeed);

        tempDX = tempSwarm.dx2;
        tempDY = tempSwarm.dy2;
        d = std::sqrt(tempDX * tempDX + tempDY * tempDY);
        if (d == 0) d = 0.001;
        tempSwarm.accelerate(tempDX * (param.normalSpeed - d) / d * param.c5,
                             tempDY * (param.normalSpeed - d) / d * param.c5,
                             param.maxSpeed);

        if (static_cast<double>(std::rand()) / RAND_MAX < mutationRateAtNormalTime) {
            if (tempSwarm.recipe != nullptr)
                tempSwarm.recipe = tempSwarm.recipe->mutate();
        }
    }
}

void SwarmPopulationSimulator::updateStates() {
    SwarmIndividual tempSwarm, tempSwarm2;
    int numberOfSwarm = swarmInBirthOrder.size();
    int j;

    for (int i = 0; i < numberOfSwarm; ++i)
        swarmInBirthOrder[i].move();

    for (int i = 1; i < numberOfSwarm; ++i) {
        tempSwarm = swarmInXOrder[i];
        j = i;
        while (j > 0) {
            tempSwarm2 = swarmInXOrder[j - 1];
            if (tempSwarm2.x > tempSwarm.x) {
                swarmInXOrder[j] = tempSwarm2;
                --j;
            } else {
                break;
            }
        }
        swarmInXOrder[j] = tempSwarm;

        tempSwarm = swarmInYOrder[i];
        j = i;
        while (j > 0) {
            tempSwarm2 = swarmInYOrder[j - 1];
            if (tempSwarm2.y > tempSwarm.y) {
                swarmInYOrder[j] = tempSwarm2;
                --j;
            } else {
                break;
            }
        }
        swarmInYOrder[j] = tempSwarm;
    }

    resetRanksInSwarm();
}

void SwarmPopulationSimulator::displayStates() {
    SwarmIndividual ag;
    int x, y;

    img.clear(sf::Color::White);

    for (auto& individual : swarmInBirthOrder) {
        x = static_cast<int>((individual.x - currentMidX) * currentScalingFactor) + width / 2;
        y = static_cast<int>((individual.y - currentMidY) * currentScalingFactor) + height / 2;
        sf::RectangleShape rectangle(sf::Vector2f(1, 1));
        rectangle.setPosition(x, y);
        rectangle.setFillColor(individual.displayColor());
        img.draw(rectangle);
    }

    img.display();
}

void SwarmPopulationSimulator::saveImage(const std::string& initial, int timestep) {
    sf::Texture texture = img.getTexture();
    sf::Image screenshot = texture.copyToImage();
    screenshot.saveToFile(initial + "-" + std::to_string(timestep) + ".jpg");
}

void SwarmPopulationSimulator::outputRecipe() {
    if (displayedRecipe == nullptr) {
        displayedRecipe = new RecipeFrame(this, swarmInBirthOrder, originalWidth, originalHeight, recipeFrames);
        recipeFrames.push_back(*displayedRecipe);
        displayedRecipe->setVisible(true);
    } else {
        displayedRecipe->putImage(img.getTexture());
        displayedRecipe->setVisible(true);
    }
}

void SwarmPopulationSimulator::draw(sf::RenderTarget& target, sf::RenderStates states) const {
    target.draw(sf::Sprite(img.getTexture()), states);
}
