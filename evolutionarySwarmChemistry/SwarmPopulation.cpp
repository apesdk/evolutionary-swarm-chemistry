// SwarmPopulation.cpp
//
// 2006-2015 (c) Copyright by Hiroki Sayama
//
// This file is part of "Evolutionary Swarm Chemistry Simulator",
// which is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// "Evolutionary Swarm Chemistry Simulator" is distributed in the hope
// that it will be useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE.  See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with "Evolutionary Swarm Chemistry Simulator".  If not, see
// <http://www.gnu.org/licenses/>.
//
// Send any correspondences to:
//   Hiroki Sayama, D.Sc.
//   Director, Center for Collective Dynamics of Complex Systems
//   Associate Professor, Department of Systems Science and Industrial Engineering
//   Binghamton University, State University of New York
//   P.O. Box 6000, Binghamton, NY 13902-6000, USA
//   Tel: +1-607-777-3566
//   Email: sayama@binghamton.edu
//
// For more information about this software, see:
//   http://bingweb.binghamton.edu/~sayama/SwarmChemistry/

#include <vector>
#include <string>
#include <cstdlib>
#include <cmath>
#include "SwarmIndividual.h"
#include "Recipe.h"

class SwarmPopulation {
public:
    std::vector<SwarmIndividual> population;
    std::string title;

    SwarmPopulation(const std::vector<SwarmIndividual>& pop, const std::string& t)
        : population(pop), title(t) {}

    SwarmPopulation(const std::string& recipeText, const std::string& t) {
        Recipe rcp(recipeText);
        population = rcp.createPopulation(300, 300);
        title = t;

        int num = 10000 - 1;

        double xx, yy;

        for (int i = 0; i < num; i++) {
            xx = (std::rand() / (double)RAND_MAX) * 5000 - 2500;
            yy = (std::rand() / (double)RAND_MAX) * 5000 - 2500;
            population.push_back(SwarmIndividual(xx + 150, yy + 150, 0, 0,
                SwarmParameters(10, 0, 0, 0, 0, 0, 0, 1)));
        }
    }

    SwarmPopulation(int n, const std::string& t) {
        population.reserve(10000);
        title = t;

        int num = 10000 - n;

        double xx, yy;

        for (int i = 0; i < n; i++) {
            xx = (std::rand() / (double)RAND_MAX) * 5000 - 2500;
            yy = (std::rand() / (double)RAND_MAX) * 5000 - 2500;
            SwarmParameters sp;
            population.push_back(SwarmIndividual(xx + 150, yy + 150, 0, 0, sp,
                new Recipe("1 * " +
                    std::to_string(shorten(sp.neighborhoodRadius)) + " " +
                    std::to_string(shorten(sp.normalSpeed)) + " " +
                    std::to_string(shorten(sp.maxSpeed)) + " " +
                    std::to_string(shorten(sp.c1)) + " " +
                    std::to_string(shorten(sp.c2)) + " " +
                    std::to_string(shorten(sp.c3)) + " " +
                    std::to_string(shorten(sp.c4)) + " " +
                    std::to_string(shorten(sp.c5)))));
        }

        for (int i = 0; i < num; i++) {
            xx = (std::rand() / (double)RAND_MAX) * 5000 - 2500;
            yy = (std::rand() / (double)RAND_MAX) * 5000 - 2500;
            population.push_back(SwarmIndividual(xx + 150, yy + 150, 0, 0,
                SwarmParameters(10, 0, 0, 0, 0, 0, 0, 1)));
        }
    }

    SwarmPopulation(const SwarmPopulation& a, const std::string& t)
        : title(t) {
        population.reserve(a.population.size());
        for (const auto& temp : a.population) {
            population.push_back(SwarmIndividual(
                (std::rand() / (double)RAND_MAX) * 5000 - 2500 + 150,
                (std::rand() / (double)RAND_MAX) * 5000 - 2500 + 150,
                (std::rand() / (double)RAND_MAX) * 10 - 5,
                (std::rand() / (double)RAND_MAX) * 10 - 5,
                SwarmParameters(temp.genome)));
        }
    }

    SwarmPopulation(const SwarmPopulation& a, const SwarmPopulation& b, double rate, const std::string& t)
        : title(t) {
        population.reserve((a.population.size() + b.population.size()) / 2);
        for (int i = 0; i < (a.population.size() + b.population.size()) / 2; i++) {
            const SwarmPopulation& source = (std::rand() / (double)RAND_MAX) < rate ? a : b;
            const SwarmIndividual& temp = source.population[(int)((std::rand() / (double)RAND_MAX) * source.population.size())];
            population.push_back(SwarmIndividual(
                (std::rand() / (double)RAND_MAX) * 5000 - 2500 + 150,
                (std::rand() / (double)RAND_MAX) * 5000 - 2500 + 150,
                (std::rand() / (double)RAND_MAX) * 10 - 5,
                (std::rand() / (double)RAND_MAX) * 10 - 5,
                SwarmParameters(temp.genome)));
        }
    }

    void perturb(double pcm, int spaceSize) {
        int pop = population.size();
        pop += (int)((std::rand() / (double)RAND_MAX * 2.0 - 1.0) * pcm * (double)pop);
        if (pop < 1) pop = 1;
        if (pop > SwarmParameters::numberOfIndividualsMax) pop = SwarmParameters::numberOfIndividualsMax;

        std::vector<SwarmIndividual> newPopulation;
        newPopulation.reserve(pop);
        SwarmParameters tempParam;
        for (int i = 0; i < pop; i++) {
            tempParam = SwarmParameters(population[(int)((std::rand() / (double)RAND_MAX) * population.size())].genome);
            newPopulation.push_back(SwarmIndividual(
                (std::rand() / (double)RAND_MAX) * spaceSize,
                (std::rand() / (double)RAND_MAX) * spaceSize,
                (std::rand() / (double)RAND_MAX) * 10 - 5,
                (std::rand() / (double)RAND_MAX) * 10 - 5,
                tempParam));
        }
        population = newPopulation;
    }

private:
    double shorten(double d) const {
        return std::round(d * 100.0) / 100.0;
    }
};

