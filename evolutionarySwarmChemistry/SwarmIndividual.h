#ifndef SWARMINDIVIDUAL_H
#define SWARMINDIVIDUAL_H

#include "SwarmParameters.h"

class SwarmIndividual {
public:
    double x, y, dx, dy;
    SwarmParameters genome;

    SwarmIndividual();
    SwarmIndividual(double xx, double yy, double dxx, double dyy, const SwarmParameters& g);
};

#endif
