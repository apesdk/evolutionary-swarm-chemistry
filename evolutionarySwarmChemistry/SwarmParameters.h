#ifndef SWARMPARAMETERS_H
#define SWARMPARAMETERS_H

class SwarmParameters {
public:
    static const int numberOfIndividualsMax = 10000;

    double neighborhoodRadius;
    double normalSpeed;
    double maxSpeed;
    double c1, c2, c3, c4, c5;

    SwarmParameters();
    SwarmParameters(double p1, double p2, double p3, double p4,
                    double p5, double p6, double p7, double p8);
    SwarmParameters(const SwarmParameters& other);

    bool operator==(const SwarmParameters& other) const;
    void inducePointMutations(double rate, double magnitude);
    void boundParameterValues();
};

#endif
