#include "SwarmParameters.h"
#include <cstdlib>
#include <cmath>

SwarmParameters::SwarmParameters()
    : neighborhoodRadius(std::rand() % 300),
      normalSpeed(std::rand() % 20),
      maxSpeed(std::rand() % 40),
      c1(static_cast<double>(std::rand()) / RAND_MAX),
      c2(static_cast<double>(std::rand()) / RAND_MAX),
      c3(static_cast<double>(std::rand()) / RAND_MAX * 100),
      c4(static_cast<double>(std::rand()) / RAND_MAX * 0.5),
      c5(static_cast<double>(std::rand()) / RAND_MAX) {
    boundParameterValues();
}

SwarmParameters::SwarmParameters(double p1, double p2, double p3, double p4,
                                 double p5, double p6, double p7, double p8)
    : neighborhoodRadius(p1),
      normalSpeed(p2),
      maxSpeed(p3),
      c1(p4), c2(p5), c3(p6), c4(p7), c5(p8) {
    boundParameterValues();
}

SwarmParameters::SwarmParameters(const SwarmParameters& other)
    : neighborhoodRadius(other.neighborhoodRadius),
      normalSpeed(other.normalSpeed),
      maxSpeed(other.maxSpeed),
      c1(other.c1), c2(other.c2), c3(other.c3), c4(other.c4), c5(other.c5) {}

bool SwarmParameters::operator==(const SwarmParameters& other) const {
    return neighborhoodRadius == other.neighborhoodRadius &&
           normalSpeed == other.normalSpeed &&
           maxSpeed == other.maxSpeed &&
           c1 == other.c1 &&
           c2 == other.c2 &&
           c3 == other.c3 &&
           c4 == other.c4 &&
           c5 == other.c5;
}

void SwarmParameters::inducePointMutations(double rate, double magnitude) {
    if (static_cast<double>(std::rand()) / RAND_MAX < rate)
        neighborhoodRadius += (static_cast<double>(std::rand()) / RAND_MAX - 0.5) * 300 * magnitude;

    if (static_cast<double>(std::rand()) / RAND_MAX < rate)
        normalSpeed += (static_cast<double>(std::rand()) / RAND_MAX - 0.5) * 20 * magnitude;

    if (static_cast<double>(std::rand()) / RAND_MAX < rate)
        maxSpeed += (static_cast<double>(std::rand()) / RAND_MAX - 0.5) * 40 * magnitude;

    if (static_cast<double>(std::rand()) / RAND_MAX < rate)
        c1 += (static_cast<double>(std::rand()) / RAND_MAX - 0.5) * 1 * magnitude;

    if (static_cast<double>(std::rand()) / RAND_MAX < rate)
        c2 += (static_cast<double>(std::rand()) / RAND_MAX - 0.5) * 1 * magnitude;

    if (static_cast<double>(std::rand()) / RAND_MAX < rate)
        c3 += (static_cast<double>(std::rand()) / RAND_MAX - 0.5) * 100 * magnitude;

    if (static_cast<double>(std::rand()) / RAND_MAX < rate)
        c4 += (static_cast<double>(std::rand()) / RAND_MAX - 0.5) * 0.5 * magnitude;

    if (static_cast<double>(std::rand()) / RAND_MAX < rate)
        c5 += (static_cast<double>(std::rand()) / RAND_MAX - 0.5) * 1 * magnitude;

    boundParameterValues();
}

void SwarmParameters::boundParameterValues() {
    if (neighborhoodRadius < 0) neighborhoodRadius = 0;
    else if (neighborhoodRadius > 300) neighborhoodRadius = 300;

    if (normalSpeed < 0) normalSpeed = 0;
    else if (normalSpeed > 20) normalSpeed = 20;

    if (maxSpeed < 0) maxSpeed = 0;
    else if (maxSpeed > 40) maxSpeed = 40;

    if (c1 < 0) c1 = 0;
    else if (c1 > 1) c1 = 1;

    if (c2 < 0) c2 = 0;
    else if (c2 > 1) c2 = 1;

    if (c3 < 0) c3 = 0;
    else if (c3 > 100) c3 = 100;

    if (c4 < 0) c4 = 0;
    else if (c4 > 0.5) c4 = 0.5;

    if (c5 < 0) c5 = 0;
    else if (c5 > 1) c5 = 1;
}
