#ifndef SWARMPOPULATIONSIMULATOR_H
#define SWARMPOPULATIONSIMULATOR_H

#include <vector>
#include <string>
#include <algorithm>
#include <cmath>
#include <ctime>
#include <cstdlib>
#include <iostream>
//#include <SFML/Graphics.hpp>
#include "SwarmIndividual.h"
#include "SwarmPopulation.h"
#include "RecipeFrame.h"

class SwarmPopulationSimulator : public sf::RenderWindow {
public:
    SwarmPopulationSimulator(int frameSize, int spaceSize, SwarmPopulation sol, 
                             std::vector<SwarmPopulation> solList, int num, 
                             bool tr, bool mo, std::vector<RecipeFrame>& rcfs);
    void simulateSwarmBehavior(const std::string& compfunc);
    void updateStates();
    void displayStates();
    void saveImage(const std::string& initial, int timestep);
    void outputRecipe();

private:
    SwarmPopulationSimulator* myself;
    int width, height, originalWidth, originalHeight;
    bool tracking, mouseEffect;

    double currentMidX, currentMidY, currentScalingFactor;
    double swarmRadius = 3; // radius of nodes
    double swarmDiameter;
    int mouseX, mouseY;
    int weightOfMouseCursor = 20;
    bool isMouseIn;

    int frameNumber;
    RecipeFrame* displayedRecipe;
    SwarmPopulation population;
    std::vector<SwarmPopulation> originalPopulationList;
    std::vector<RecipeFrame>& recipeFrames;

    sf::Image im;
    sf::RenderTexture img;
    std::vector<SwarmIndividual> swarmInBirthOrder, swarmInXOrder, swarmInYOrder;
    bool isSelected, notYetNoticed;

    double mutationRateAtTransmission = 0.1; // *** to be modified ***
    double mutationRateAtNormalTime = 0.001; // *** to be modified ***

    void clearImage();
    void addSwarm(SwarmIndividual b);
    void replacePopulationWith(SwarmPopulation newpop);
    void resetRanksInSwarm();
    bool losing(SwarmIndividual defender, SwarmIndividual attacker, const std::string& compfunc);
    std::vector<SwarmIndividual> neighborsOf(SwarmIndividual tempSwarm, double radius);

protected:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
};

#endif
